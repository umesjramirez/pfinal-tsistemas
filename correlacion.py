import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as waves

def extraeuncanal(sonido):
    canales=sonido.shape
    cuantos=len(canales)
    canal=0
    if(cuantos==1):
        uncanal=sonido[:]
    if(cuantos>=2):
        uncanal=sonido[:,canal]
    return(uncanal)
    


# COMPARACION
archivo01 = 'rectest.wav'
archivo02 = 'rectest2.wav'

muestreo, sonido = waves.read(archivo01)
senal01 = extraeuncanal(sonido)

muestreo,sonido = waves.read(archivo02)
senal02 = extraeuncanal(sonido)


#PROCEDIMIENTO
tamano01 = len(senal01)
tamano02 = len(senal02)

#normalizacion de seniales
amplitud= np.max(senal01)
senal01 = senal01 / amplitud
senal02 = senal02 / amplitud

# correlacion para comparar
correlacion = np.correlate(senal01,senal02,mode='same')

#SALIDA
extra = np.abs(tamano01-tamano02)
relleno = np.zeros(extra,dtype=float)
# senal01relleno = np.concatenate((senal01,relleno),axis=0)
plt.suptitle('correlacion (senal01,senal02)')

plt.subplot(311)
plt.plot(senal01,'g',label='senal01')
plt.legend()

plt.subplot(312)
plt.plot(senal02,'b',label = 'senal02')
plt.legend()

plt.subplot(313)
plt.plot(correlacion,'m',label='correlacion')
plt.legend()

plt.show()