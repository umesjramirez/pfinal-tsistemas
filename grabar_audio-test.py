import pyaudio
import wave

form_1 = pyaudio.paInt16 # resolucion de 16-bit
chans = 1 # 1 canal
fs = 44100 # frecuencia de muestreo
muestra = 4096 #2 /\ 12 muestras por buffer
tiempo_grabacion =3
dev_index =0 # indice del microfono
wav_output_archivo = 'test1.wav' # archivo wav

audio = pyaudio.PyAudio()

stream = audio.open(format = form_1, rate = fs,channels = chans, \
                    input_device_index = dev_index, input = True, \
                    frames_per_buffer = muestra)

print ("grabando")
grabacion = []

#ciclo a travez del 'stream' de pyaudio para agregar muestras de audio al array 'audio'
for ii in range(0,int((fs/muestra)*tiempo_grabacion)):
    data = stream.read(muestra)
    grabacion.append(data)
    
print("grabacion terminada")

# se detiene el stream, se cierra y se termina la instancia de pyaudio
stream.stop_stream()
stream.close()
audio.terminate()

# guardar el archivo de audio en formato wav
wavefile = wave.open(wav_output_archivo,'wb')
wavefile.setnchannels(chans)
wavefile.setsampwidth(audio.get_sample_size(form_1))
wavefile.setframerate(fs)
wavefile.writeframes(b''.join(grabacion))
wavefile.close()

# COMPARACION
