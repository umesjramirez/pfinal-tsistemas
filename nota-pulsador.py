
import gpiozero as gpio
import time
import tkinter as tk
import tkSnack

def setVolume(volume=50):
    """set the volume of the sound system"""
    if volume > 100:
        volume = 100
    elif volume < 0:
        volume = 0
    tkSnack.audio.play_gain(volume)
    
def playNote(freq, duration):
    """play a note of freq (hertz) for duration (seconds)"""
    snd = tkSnack.Sound()
    filt = tkSnack.Filter('generator', freq, 30000, 0.0, 'sine', int(11500*duration))
    snd.stop()
    snd.play(filter=filt, blocking=1)
def soundStop():
    """stop the sound the hard way"""
    try:
        root = root.destroy()
        filt = None
    except:
        pass


    
# have to initialize the sound system, required!!

# play a note of requency 440 hertz (A4) for a duration of 5 seconds
#playNote(440, 1)
# play a note of requency 261.6 hertz (C4) for a duration of 5 seconds

 #  f = 261.63  # Do
 #  f = 293.66  # RE
  # f = 329.63  # MI
  # f = 349.23  # FA
  # f = 392  # SOL
  # f = 440  # LA
  # f = 493.88  # SI

do = gpio.Button(14) #pin 11
re = gpio.Button(15) #pin 11
mi = gpio.Button(18) #pin 11
fa = gpio.Button(23) #pin 11
sol = gpio.Button(24) #pin 11
la = gpio.Button(25) #pin 11
si = gpio.Button(8) #pin 11


while True:
        if do.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("do")
            playNote(261.63, 1)
            # optional
            soundStop()
            root.withdraw()
            
        if re.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("re")
            playNote(293.66, 1)
            # optional
            soundStop()
            root.withdraw()
            
        #time.sleep(1)
        if mi.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("mi")
            playNote(329.63, 1)
            # optional
            soundStop()
            root.withdraw()
    
        if fa.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("fa")
            playNote(349.23, 1)
            # optional
            soundStop()
            root.withdraw()
            
            
        if sol.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("sol")
            playNote(392, 1)
            # optional
            soundStop()
            root.withdraw()
            
        if la.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("la")
            playNote(440, 1)
            # optional
            soundStop()
            root.withdraw()
        
        if si.is_pressed:
            print("no presionado1")
        else:
            root = tk.Tk()
            tkSnack.initializeSnack(root)
           
            setVolume(80)
            print("si")
            playNote(493.88, 1)
            # optional
            soundStop()
            root.withdraw()
       