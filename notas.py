#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
import pyaudio

# Definimos una funciÃ³n senoidal simple.
def ondasimple(t,f):
  A = 1.0    # amplitud
#   f = 500.0  # frequencia
#   f = 261.63  # Do
#   f = 293.66  # RE
  # f = 329.63  # MI
  # f = 349.23  # FA
  # f = 392  # SOL
  # f = 440  # LA
  # f = 493.88  # SI
  Phi = 0.0  # fase
  return A * np.sin(2 * np.pi * f * t + Phi)

f = 261.63  # Do
fs = 44100
# Generamos 16000 puntos a 16kHz.
ts = np.arange(16000.0) / 16000.0

# Armamos una onda senoidal discretizada.
mionda = []
for t in ts:
  mionda.append(ondasimple(t,f))
mionda = np.array(mionda)


# Graficamos la onda.
# import matplotlib.pyplot as pyplot
# pyplot.clf()
# pyplot.plot(ts[0:100], mionda[0:100])
# pyplot.savefig('si.png')


# La guardamos como wav.
import scipy.io.wavfile as syipwave
wavdata = np.array(mionda * 10000.0, dtype=np.float32)
syipwave.write('do.wav', 16000, wavdata)

# reproducir audio
p= pyaudio.PyAudio()
x = (np.sin(2*np.pi*np.arange(fs)*f/fs))
x = np.float32(x)

stream = p.open(format = pyaudio.paFloat32,
            channels =1,
            rate = fs,
            output = True
            )
stream.write(x)
stream.stop_stream()

stream.close()

p.terminate()