#!/usr/bin/python
#Harp.py
#DESCRIPTION: Play an LED Harp
#
#AUTHOR: Ken Fisher Family Christmas Project #
#Raspberry Pi Model B Rev 2 (000e)
#IRQ for both Note ON and Note OFF???
#
import pygame.mixer
import pygame.midi
import time
import random
import RPi.GPIO as GPIO
#PYGAME mixer and midi SETUP
#pygame.mixer.init()
pygame.init()
pygame.midi.init()
port = 2
latency = 1
instrument = 42 # Acoustic Grand Piano is 0 
velocity = 127 # Max Volume is 127
baseNote = 60 #Middle C is 60

midiOutput = pygame.midi.Output(port, latency) 
midiOutput.set_instrument(instrument)
#LDR status for note length--add 0 based item so 1 based works 
ldr_status = {}
#GPIO SETUP 
GPIO.setmode(GPIO.BCM) 
GPIO.setwarnings(False) 
gpio_ldr = {

#GPIO #: Note/Offset/Key PIN NAME LABEL
# Note/Key 15-16 Octave Down/Up; 20-2: Menu
    #1 3.3V LDR Bank1
    #2 5V LED Bank1
#2: 20, #3 GPIO2 Menu30 (NOTE: 1.8kOhm external pull up resistor) - LED Activ HIGH, 100?? 
    #4 5V LED Bank2
#3: 21, #5 GPIO3 Menu31 (NOTE: 1.8kOhm external pull up resistor) - LED??
#6 GND LED Bank1
4: 15, #7 GPIO4 Menu32
14: 17, #8 GPIO14 OctiveUp21 
    #9 GND LED Bank1
15: 5, #10  GPIO15 Note06
17: 0, #11  GPIO17 Note01
18: 6, #12  GPIO18 Note07
27: 1, #13  GPIO27 Note02
    #14 GND LED Bank2
22: 2, #15 GPIO22 Note03
23: 7, #16 GPIO23 Note08
    #17 3.3V LDR Bank2
24: 8, #18 GPIO24 Note09
10: 3, #19 GPIO10 Note04 
    #20 GND
9: 4, #21  GPIO9 Note05
25: 9, #22 GPIO25 Note10
11: 16, #23 GPIO11 OctiveDown20
8: 10, #24 GPIO8 Note11
#20 GND
7: 11, #26 GPIO7 Note12 

}
#Define Interupt that will start midi note 
def gpio_ldr_irq(pin):
    global ldr_ptr
    global gpio_ldr
    global ldr_status
    print ("NOTE ON: %d, PIN: %d, STATUS: %d" % (gpio_ldr[pin],pin,ldr_status[pin])) 
    if (pin != 14) and (pin != 11):
        if (ldr_status[pin] == 0):
            ldr_status[pin] = 1
            midiOutput.note_on(baseNote + gpio_ldr[pin],velocity)
        toExit = 0

ldr_ptr = 0
starttime = 0
toExit = 0
loopDelay = .25 
#SETUP GPIO interface 
for pin in gpio_ldr:
#GPIO.setup(pin, GPIO.IN, GPIO.PUD_UP) 
    GPIO.setup(pin, GPIO.IN)
    GPIO.add_event_detect(pin, GPIO.RISING, gpio_ldr_irq, 250) #Debuonce? 
    ldr_status[pin] = 0

#SETUP RED LED indicators 
GPIO.setup(2, GPIO.OUT) 
GPIO.output(2, 1)

print ("Setup complete-Hold Note for 10 seconds to quit")

timeStamp = time.time()
print ("Local Current Time: ", time.asctime(time.localtime()))


#Main Loop 
inputLoop = True 
while inputLoop:
    time.sleep(loopDelay) #Delay to allow Pygame MIDI to play note properly 
    for pin in gpio_ldr:
        if ldr_status[pin] == 1:
            if GPIO.input(pin) == 1:
                toExit +=  s1
                #print ("toExit %d: %d" % (gpio_ldr[pin],toExit)) 
                if toExit >= (3/loopDelay):
                    inputLoop = False
                    print ("toExit %d: %d" % (gpio_ldr[pin],toExit)) 
                if GPIO.input(pin) == 0:
                    toExit = 0
                    print ("NOTE OFF %d" % pin) 
                    midiOutput.note_off(baseNote+gpio_ldr[pin],velocity) 
                    idiOutput.note_off(baseNote+gpio_ldr[pin],velocity) # In case debounce failed 
                    ldr_status[pin] = 0
                if GPIO.input(14) == 1: 
                    baseNote = baseNote + 12 
                    if baseNote >= 108:
                        baseNote = 108
                if GPIO.input(11) == 1:
                    baseNote = baseNote - 12 
                    if baseNote <= 0:
                        baseNote = 0

#CLEANUP and SHUTDOWN 
del midiOutput 
pygame.midi.quit() 
GPIO.output(2, 0) #LED Off

GPIO.cleanup() 
exit()