import speech_recognition as sr

r = sr.Recognizer()

with sr.Microphone() as source:
    print("Diga algo: ")
    
    audio = r.listen(source)
    
    try:
        text = r.recognize_google(audio)
        
        print(" Usted dijo : {} ".format(text))
        
    except:
        
        print("No se pudo reconocer su voz")