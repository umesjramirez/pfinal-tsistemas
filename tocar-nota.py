# play a note of a given frequency and duration via the PC sound card
# needs Python module  tkSnack  developed at KTH in Stockholm, Sweden
# free download: snack229-py.zip
# from: http://www.speech.kth.se/snack/
# on my system I copied tkSnack.py to D:\Python24\Lib and the folder snacklib to D:\Python24\tcl
# tested with Python24     vegaseat     28oct2005
#   f = 500.0  # frequencia
#   f = 261.63  # Do
#   f = 293.66  # RE
  # f = 329.63  # MI
  # f = 349.23  # FA
  # f = 392  # SOL
  # f = 440  # LA
  # f = 493.88  # SI
import tkinter as tk
import tkSnack
def setVolume(volume=50):
    """set the volume of the sound system"""
    if volume > 100:
        volume = 100
    elif volume < 0:
        volume = 0
    tkSnack.audio.play_gain(volume)
def playNote(freq, duration):
    """play a note of freq (hertz) for duration (seconds)"""
    snd = tkSnack.Sound()
    filt = tkSnack.Filter('generator', freq, 30000, 0.0, 'sine', int(11500*duration))
    snd.stop()
    snd.play(filter=filt, blocking=1)
def soundStop():
    """stop the sound the hard way"""
    try:
        root = root.destroy()
        filt = None
    except:
        pass
    
    
#root = Tkinter.Tk()
root = tk.Tk()
    
# have to initialize the sound system, required!!
tkSnack.initializeSnack(root)
# set the volume of the sound system (0 to 100%)
setVolume(30)
# play a note of requency 440 hertz (A4) for a duration of 5 seconds
#playNote(440, 1)
# play a note of requency 261.6 hertz (C4) for a duration of 5 seconds
playNote(261.63, 1)
# optional
soundStop()
    
root.withdraw()